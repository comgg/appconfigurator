package com.gg.configurations;

/**
 * This interface is used to force the user to follow certain steps.
 */
public interface ConfigurationReloadCallBack {
	/**
	 * You must implement this inside your code base if you want to execute code
	 * before finishing the configuration.
	 */
	void callBack();
}
