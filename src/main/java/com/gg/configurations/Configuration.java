package com.gg.configurations;

import com.gg.parameters.Parameter;
import com.gg.utils.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * This class implements the configuration steps required to create a configuration,
 */
public class Configuration implements ConfigurationStep, CompleteStep {
	private static final Logger LOGGER = Logger.getLogger(Configuration.class.getName());
	/**
	 * Configuration's name
	 */
	private final String name;
	/**
	 * Configuration's key/value parameters
	 */
	private final Map<String, Parameter> parameters = new HashMap<>();
	/**
	 * If there's a xml file present, these properties are its contents
	 */
	private Properties properties;
	private ConfigurationReloadCallBack callBack;

	public Configuration(String name) {
		this.name = name;
	}

	@Override
	public ConfigurationStep addRequiredParameter(String parameterName) {
		Parameter parameter = new Parameter(name, parameterName, true);
		parameters.merge(parameter.getName(), parameter, (currentValue, newValue) -> newValue);
		return this;
	}

	@Override
	public ConfigurationStep addOptionalParameter(String parameterName) {
		Parameter parameter = new Parameter(name, parameterName, false);
		parameters.merge(parameter.getName(), parameter, (currentValue, newValue) -> newValue);
		return this;
	}

	@Override
	public CompleteStep addConfigurationReloadCallBack(ConfigurationReloadCallBack callBack) {
		this.callBack = callBack;
		return this;
	}

	@Override
	public void complete(boolean writeConfiguration) {
		File configurationFile = FileUtils.getConfigurationFile(name, true);
		properties = FileUtils.readConfigurationFile(configurationFile);
		// properties will be null if configurationFile is null
		if (properties == null && writeConfiguration) {
			// creates new properties and dumps all parameters and their values
			properties = new Properties();
			// this writes configuration file with all existing parameters with empty value
			parameters.forEach((key, value) -> properties.setProperty(key, ""));
			writeProperties(properties, configurationFile);
			// this rewrites the created configuration file if there are any variables with values
			parameters.entrySet().stream()
					.filter(stringParameterEntry -> stringParameterEntry.getValue().getAsString() != null)
					.forEach(stringParameterEntry -> properties.setProperty(stringParameterEntry.getKey(), stringParameterEntry.getValue().getAsString()));
			writeProperties(properties, configurationFile);
		} else {
			// creates new properties dumps all parameters and puts old values
			Properties newProperties = new Properties();
			parameters.forEach((s, parameter) -> {
				if (properties.containsKey(s))
					newProperties.setProperty(s, properties.getProperty(s));
				else if (parameter.getAsString() != null)
					newProperties.setProperty(s, parameter.getAsString());
			});
			writeProperties(newProperties, configurationFile);
		}
		if(callBack != null)
			callBack.callBack();
	}

	/**
	 * Method for writing properties to xml including checks
	 */
	private void writeProperties(Properties properties, File configurationFile) {
		FileOutputStream os = null;
		try {
			os = new FileOutputStream(configurationFile);
			properties.storeToXML(os, name);
			LOGGER.info(String.format("Wrote configuration to: %s", configurationFile.getAbsolutePath()));
		} catch (FileNotFoundException | SecurityException | NullPointerException e) {
			LOGGER.severe(String.format("File: %s cannot be found or you don't have permission", configurationFile.getAbsolutePath()));
		} catch (IOException | ClassCastException e) {
			LOGGER.severe("Something horribly wrong has happened");
			LOGGER.severe(e.getMessage());
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					LOGGER.severe("Something horribly wrong has happened");
					LOGGER.severe(e.getMessage());
				}
			}
		}
	}

	public void reload() {
		this.callBack.callBack();
	}

	public String getName() {
		return name;
	}

	public Map<String, Parameter> getParameters() {
		return parameters;
	}

	public Parameter getParameter(String parameterName) {
		return parameters.get(parameterName);
	}

	public Properties getProperties() {
		return properties;
	}
}
