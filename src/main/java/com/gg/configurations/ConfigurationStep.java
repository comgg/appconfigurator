package com.gg.configurations;

/**
 * This interface is used to force the user to follow certain steps.
 */
public interface ConfigurationStep {

	/**
	 * Adds required parameter
	 */
	ConfigurationStep addRequiredParameter(String parameterName);

	/**
	 * Adds optional parameter
	 */
	ConfigurationStep addOptionalParameter(String parameterName);

	/**
	 * Add configuration reload callback inside the user's code base.
	 * It is required to be called even with empty implementation in order
	 * to complete the configuration.
	 * In addition, there is writeConfigurationToFile parameter which lets you
	 * save all your parameters to xml file even if they are not defined.
	 */
	CompleteStep addConfigurationReloadCallBack(ConfigurationReloadCallBack callBack);

}
