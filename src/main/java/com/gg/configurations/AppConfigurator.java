package com.gg.configurations;

import java.util.HashMap;
import java.util.Map;

/**
 * Use this method to create new configurations and access them.
 */
public class AppConfigurator {
	/**
	 * Configurations stored as key/configuration.
	 */
	private static final Map<String, Configuration> configurations = new HashMap<>();

	/**
	 * Call this method with your configuration name, and it will be stored in
	 * configuration. If there's already configuration with this name you will
	 * overwrite it.
	 */
	public static ConfigurationStep configure(String configurationName) {
		Configuration configuration = new Configuration(configurationName);
		configurations.merge(configuration.getName(), configuration, (currentValue, newValue) -> newValue);
		return configuration;
	}

	/**
	 * Get configuration from stored configurations by its name.
	 */
	public static Configuration getConfiguration(String configurationName) {
		return configurations.get(configurationName);
	}
}
