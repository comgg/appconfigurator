package com.gg.configurations;

/**
 * This interface is used to force the user to follow certain steps.
 */
public interface CompleteStep {
	/**
	 * This is the final step of the configuration. Calling this method
	 * will finish the configuration making it not possible to add more
	 * parameters.
	 */
	default void complete() {
		complete(false);
	}
	void complete(boolean writeConfiguration);
}
