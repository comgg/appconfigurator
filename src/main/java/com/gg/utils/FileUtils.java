package com.gg.utils;

import java.io.*;
import java.util.HashMap;
import java.util.InvalidPropertiesFormatException;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

public class FileUtils {
	
	private static final Logger LOGGER = Logger.getLogger(FileUtils.class.getName());

	/**
	 * This is the default name of the parameter used to provide configuration file's path
	 * Example:
	 * -Dconfiguration=/full/path # this is passed to JVM
	 * configuration=/full/path # this is system or user environment variable
	 */
	private final static String CONFIGURATION_PARAMETER = "configuration";

	/**
	 * This is default parameter name for tomcat's base folder property
	 */
	private final static String CATALINA_BASE_PARAMETER = "catalina.base";

	/**
	 * Tomcat's default application's folder
	 */
	private final static String CATALINA_WEBAPPS = "webapps";

	/**
	 * This is java default property representing current working directory.
	 * It represents the folder from which your application is running from.
	 * In tomcat's case that's CATALINA_BASE/bin
	 */
	private final static String WORKING_DIRECTORY_PARAMETER = "user.dir";

	/**
	 * This map contains configurations which are already read by radConfigurationFile method
	 */
	private final static Map<File, Properties> readConfigurations = new HashMap<>();
	/**
	 * This map contains configurations which are already read by getConfigurationFile method
	 */
	private final static Map<String, File> readConfigurationFiles = new HashMap<>();

	/**
     * This method reads the value of CONFIGURATION_PARAMETER, adds the passed configuration parameter
     * as filename to CONFIGURATION_PARAMETER's path value and reads the file.
     * <p>The XML document must have the following DOCTYPE declaration:
     * <pre>
     * &lt;!DOCTYPE properties SYSTEM "<a href="http://java.sun.com/dtd/properties.dtd">...</a>"&gt;
     * </pre>
     * If the file cannot be read returns null
     */
	public static Properties readConfigurationFile(File configurationFile) {
		if(configurationFile == null)
			return null;
		if(readConfigurations.containsKey(configurationFile))
			return readConfigurations.get(configurationFile);
		Properties properties = new Properties();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(configurationFile);
			properties.loadFromXML(fis);
			readConfigurations.merge(configurationFile, properties, (c, n) -> n);
		} catch (NullPointerException e) {
			LOGGER.info("configuration parameter/environment is not defined");
			properties = null;
		} catch (FileNotFoundException | SecurityException e) {
			LOGGER.severe(String.format("Configuration file %s is missing or it cannot be read", configurationFile.getAbsolutePath()));
			properties = null;
		} catch (InvalidPropertiesFormatException e) {
			LOGGER.warning(String.format("The configuration %s you provided is not valid xml. If this is new file it's normal.", configurationFile.getAbsolutePath()));
			properties = null;
		} catch (UnsupportedEncodingException e) {
			LOGGER.severe(String.format("Character encoding for %s configuration not supported", configurationFile.getAbsolutePath()));
			properties = null;
		} catch (IOException e) {
			LOGGER.severe("Something terribly wrong has happened");
			LOGGER.severe(e.getMessage());
			properties = null;
		} finally {
			if(fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					LOGGER.severe("Something terribly wrong has happened");
					LOGGER.severe(e.getMessage());
				}
			}
		}
		return properties;
	}

	public static File getConfigurationFile(String configuration) {
		return getConfigurationFile(configuration, false);
	}

	/**
	 * Returns configuration file. Pass true to create parameter it creates one.
	 */
	public static File getConfigurationFile(String configuration, boolean create) {
		if(configuration == null)
			return null;
		if(readConfigurationFiles.containsKey(configuration))
			return readConfigurationFiles.get(configuration);
		// needed to prevent recurse
		File configurationDir = null;
		File configurationFile = null;
		// read all available properties
		String currentWorkingDirectory = ParameterUtils.readParameter(WORKING_DIRECTORY_PARAMETER); // current working directory
		String catalinaBaseDirectory = ParameterUtils.readParameter(CATALINA_BASE_PARAMETER); // catalina base folder
		String configurationDirectory = ParameterUtils.readParameter(CONFIGURATION_PARAMETER); // user defined folder
		// check all available properties
		if(currentWorkingDirectory != null
				&& !currentWorkingDirectory.isEmpty()) {
			File cwd = new File(currentWorkingDirectory);
			// execute is necessary for directories to be opened
			if(cwd.exists() && cwd.canRead() && cwd.canWrite() && cwd.canExecute())
				configurationDir = cwd;
		}
		if(catalinaBaseDirectory != null
				&& !catalinaBaseDirectory.isEmpty()) {
			File catalina = new File(catalinaBaseDirectory + File.separator + CATALINA_WEBAPPS);
			// execute is necessary for directories to be opened
			if(catalina.exists() && catalina.canRead() && catalina.canWrite() &&  catalina.canExecute())
				configurationDir = catalina;
		}
		if(configurationDirectory != null
				&& !configurationDirectory.isEmpty()) {
			File conf = new File(configurationDirectory);
			// execute is necessary for directories to be opened
			if(conf.exists() && conf.canRead() && conf.canWrite() && conf.canExecute())
				configurationDir = conf;
		}
		// execute is necessary for directories to be opened
		if(configurationDir != null
				&& configurationDir.canExecute()
				&& configurationDir.canWrite()
				&& configurationDir.canRead()) {
			configurationFile = new File(configurationDir.getAbsolutePath()+File.separator+configuration);
			try {
				if(!configurationFile.exists() && create && configurationFile.createNewFile())
					LOGGER.info(String.format("Configuration file created at: %s", configurationFile.getAbsolutePath()));
			} catch (IOException e) {
				LOGGER.severe(e.getMessage());
			}
			readConfigurationFiles.merge(configuration, configurationFile, (c, n) -> n);
		}
		return configurationFile;
	}

}
