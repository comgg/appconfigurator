package com.gg.utils;

import com.gg.parameters.Parameter;

import java.io.*;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * This class provides methods for reading environmental variables,
 * User/System/JVM properties and xml defined properties.
 */
public class ParameterUtils {
	
	private static final Logger LOGGER = Logger.getLogger(ParameterUtils.class.getName());

	/**
	 * Reads environmental variable
	 */
	public static String readEnv(String variable) {
		return readEnv(variable, false);
	}
	public static String readEnv(String variable, boolean required) {
		String value;
		try {
			value = System.getenv(variable);
			if (value == null || value.isEmpty())
				throw new IllegalArgumentException();
		} catch (SecurityException s) {
			LOGGER.severe(String.format("You don't have permission to read %s parameter", variable));
			value = null;
			if (required)
				throw new RuntimeException(String.format("You don't have permission to read %s parameter and it is required", variable));
		} catch (IllegalArgumentException a) {
			value = null;
			if (required)
				throw new RuntimeException(String.format("%s parameter is empty but it is required", variable));
		}
		return value;
	}

	/**
	 * Reads property from the passed properties and if it's null
	 * reads argument passed to JVM or predefined system properties.
	 */
	public static String readProperty(String property) {
		return readProperty(property, false);
	}
	public static String readProperty(String property, boolean required) {
		return readProperty(System.getProperties(), property, required);
	}
	public static String readProperty(Properties properties, String property, boolean required) {
		String value;
		try {
			value = properties.getProperty(property);
		} catch (SecurityException s) {
			LOGGER.severe(String.format("You don't have permission to read %s property", property));
			value = null;
			if (required)
				throw new RuntimeException(String.format("You don't have permission to read %s property and it is required", property));
		} catch (NullPointerException | IllegalArgumentException n) {
			value = null;
			if (required)
				throw new RuntimeException(String.format("Property %s key/value cannot be empty", property));
		}
		return value;
	}

	/**
	 * Combined method for reading file/property/environment variable exactly in this order.
	 * Environment variable has the highest priority, then property and finally file contents.
	 * If configuration parameter is null then only system properties and environment variables are read
	 */
	public static String readParameter(String parameter) {
		return readParameter(parameter, false);
	}
	public static String readParameter(String parameter, boolean required) {
		return readParameter(null, parameter, required);
	}
	public static String readParameter(String configuration, String parameter, boolean required) {
		String value = null;
		try {
			// file
			Properties propertiesReadFromXml = null;
			if(configuration != null)
				propertiesReadFromXml = FileUtils.readConfigurationFile(FileUtils.getConfigurationFile(configuration, false));
			if (propertiesReadFromXml != null)
				value = readProperty(propertiesReadFromXml, parameter, required);
			// argument
			value = readProperty(parameter) != null ? readProperty(parameter) : value;
			// environment
			value = readEnv(parameter) != null ? readEnv(parameter) : value;
			// checks
			if (value == null || value.isEmpty()) throw new IllegalArgumentException();
		} catch (IllegalArgumentException a) {
			value = null;
			if (required) {
				LOGGER.severe(String.format("Parameter %s cannot have empty key/value", parameter));
				throw new RuntimeException();
			}
		}
		return value;
	}

	/**
	 * Read parameter as string
	 */
	public static String getParameterAsString(Parameter parameter) {
		return ParameterUtils.readParameter(parameter.getConfiguration(), parameter.getName(), parameter.isRequired());
	}

	/**
	 * Read parameter as integer
	 * If it's not possible to convert the parameter to integer it returns -1
	 */
	public static Integer getParameterAsInteger(Parameter parameter) {
		try {
			return Integer.valueOf(ParameterUtils.readParameter(parameter.getConfiguration(), parameter.getName(), parameter.isRequired()));
		} catch (NumberFormatException | NullPointerException e) {
			return -1;
		}
	}

	/**
	 * Read parameter as boolean
	 * Possible values are:
	 * yes/y/true - lower,upper,camelcase
	 * no/n/false - lower,upper,camelcase
	 * If it's not possible to be read as string it returns false. Keep that in mind ;)
	 */
	public static boolean getParameterAsBoolean(Parameter parameter) {
		switch (ParameterUtils.readParameter(parameter.getName(), parameter.getName(), parameter.isRequired()).toLowerCase(Locale.ROOT)) {
			case "yes":
			case "y":
			case "true":
				return true;
			case "no":
			case "n":
			case "false":
				return false;
		}
		return false;
	}

	/**
	 * Read parameter as float
	 * If it's not possible to convert the parameter to float it returns -1
	 */
	public static Float getParameterAsFloat(Parameter parameter) {
		try {
			return Float.valueOf(ParameterUtils.readParameter(parameter.getConfiguration(), parameter.getName(), parameter.isRequired()));
		} catch (NumberFormatException | NullPointerException e) {
			return -1F;
		}
	}

	/**
	 * If the parameter is valid path it returns file else it returns null
	 */
	public static File getParameterAsFile(Parameter parameter) {
		String path = ParameterUtils.readParameter(parameter.getConfiguration(), parameter.getName(), parameter.isRequired());
		if (path == null)
			return null;
		else
			return new File(path);
	}

	/**
	 * Returns file contents if file is valid else it returns empty string
	 */
	public static String getParameterFileContents(Parameter parameter) {
		BufferedReader reader = null;
		File f = getParameterAsFile(parameter);
		if (f == null) return "";
		try {
			StringBuilder sb = new StringBuilder();
			reader = new BufferedReader(new FileReader(f));
			reader.lines().forEach(sb::append);
			return sb.toString();
		} catch (FileNotFoundException e) {
			LOGGER.severe(String.format("%s not found or cannot be read", parameter.getName()));
		} finally {
			try {
				if (reader != null)
					reader.close();
			} catch (IOException e) {
				LOGGER.severe(e.getMessage());
			}
		}
		return "";
	}
}
