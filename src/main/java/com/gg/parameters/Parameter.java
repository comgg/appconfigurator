package com.gg.parameters;

import com.gg.utils.ParameterUtils;

import java.io.File;

/**
 * This class is representation of your defined parameters
 */
public class Parameter {
	/**
	 * Configuration's name this parameter belongs to.
	 * It is necessary in order to save it inside a file.
	 */
	private final String configuration;
	/**
	 * Parameter's name
	 */
	private final String name;
	/**
	 * Shows if parameter is required or not.
	 * Default value is false
	 */
	private final boolean required;

	public Parameter(String configuration, String name, boolean required) {
		this.configuration = configuration;
		this.name = name;
		this.required = required;
	}

	/**
	 * Get read parameter as string
	 */
	public String getAsString() {
		return ParameterUtils.getParameterAsString(this);
	}

	/**
	 * Get read parameter as integer
	 */
	public Integer getAsInteger() {
		return ParameterUtils.getParameterAsInteger(this);
	}

	/**
	 * Get read parameter as float
	 */
	public Float getAsFloat() {
		return ParameterUtils.getParameterAsFloat(this);
	}

	/**
	 * Get read parameter as boolean
	 */
	public boolean getAsBoolean() {
		return ParameterUtils.getParameterAsBoolean(this);
	}

	/**
	 * Get read parameter path as file
	 */
	public File getAsFile() {
		return ParameterUtils.getParameterAsFile(this);
	}

	/**
	 * Get read parameter file contents
	 */
	public String readFileContents() {
		return ParameterUtils.getParameterFileContents(this);
	}

	public String getConfiguration() {
		return configuration;
	}

	public String getName() {
		return name;
	}

	public boolean isRequired() {
		return required;
	}

}
