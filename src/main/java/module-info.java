module appconfigurator {
    requires java.logging;
    exports com.gg.configurations;
    exports com.gg.parameters;
}